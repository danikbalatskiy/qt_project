#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtSql>
#include <QSqlDatabase>
#include <QUrl>
#include <QUrlQuery>
#include <QDesktopServices>
#include <QCommandLinkButton>
#include <QSqlResult>
#include <QKeyEvent>


QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

protected:
    void keyPressEvent(QKeyEvent* e) override;

private slots:
    void on_create_link_clicked();

    void on_scrollArea_customContextMenuRequested(const QPoint &pos);

    void on_commandLinkButton_clicked();

    void open_url() {

        QString name = qobject_cast<QCommandLinkButton*>(sender())->toolTip(); // получаем данные
           QStringList full_url = name.split(": ");
           // Если нет слова "http" в ссылке, то добавляем его
           if(!full_url.contains("http")) full_url[1] = "http://" + full_url[1];
           QDesktopServices::openUrl(QUrl(full_url[1]));

    };


private:
    Ui::MainWindow *ui;
    QSqlDatabase db;
};
#endif // MAINWINDOW_H
