#include "mainwindow.h"
#include "qcommandlinkbutton.h"
#include "qdesktopservices.h"
#include "qlabel.h"
#include "qnamespace.h"
#include "ui_mainwindow.h"
#include <QMessageBox>
#include <QLabel>
#include <QFrame>
#include <QString>
#include <QUrl>
#include <QUrlQuery>
#include <QDesktopServices>
#include <QCommandLinkButton>
#include <QSqlResult>





//QString link = "http://www.google.com";
// QDesktopServices::openUrl(QUrl(shortlink));


MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)





{
    ui->setupUi(this);

    db = QSqlDatabase::addDatabase("QMYSQL");
    db.setHostName("localhost");
    db.setUserName("root");
    db.setPassword("root");
    db.setPort(3306);
    db.setDatabaseName("short_links");

    if(!db.open())
        QMessageBox::critical(this, "Ошибка", "Подключение к базе данных не выполнено");
    else {
        QSqlQuery query;
        if(!query.exec("SELECT * FROM links ORDER BY id DESC"))
             QMessageBox::critical(this, "Ошибка", "Error error");
        else {
            ui->verticalLayout->setAlignment(Qt::AlignTop);
            while(query.next()){

               QCommandLinkButton* shortlink = new QCommandLinkButton(query.value("shortlink").toString());

               shortlink->setToolTip("Перейти по ссылке: " + query.value("longlink").toString());

               QObject::connect(shortlink, &QPushButton::clicked, this, &MainWindow::open_url);

               QFrame* line = new QFrame(this);

               line->setFrameShape(QFrame::HLine);

               shortlink->setProperty("class", "shortlink");

               ui->verticalLayout->addWidget(shortlink);
               ui->verticalLayout->addWidget(line);



            }




    }
}




    db.close();
}




MainWindow::~MainWindow()
{
    delete ui;
}








void MainWindow::on_create_link_clicked()
{

    QString longlink = ui->line_longlink->text();
    QString shortlink = ui->line_shortlink->text();


    if(longlink.isEmpty()){
        ui->create_link->setText("Ссылка не введена");
       // QMessageBox::information(this,"Error", "Longlink is empty");
        return;
    }

    if(!longlink.contains(".")){
         ui->create_link->setText("Ссылка должна содержать точку");
         return;
    }

    if(shortlink.isEmpty()){
         ui->create_link->setText("Сокращение для ссылки не введено");
       // QMessageBox::information(this,"Error", "Shortlink is empty");
        return;
    }

    db = QSqlDatabase::addDatabase("QMYSQL");
    db.setHostName("localhost");
    db.setUserName("root");
    db.setPassword("root");
    db.setPort(3306);
    db.setDatabaseName("short_links");




   if(!db.open())
        QMessageBox::critical(this, "Ошибка", "Подключение к базе данных не выполнено");
    else{
        QSqlQuery query;
        QString execute = "SELECT id FROM links WHERE shortlink = :shortlink";
        query.prepare(execute);
        query.bindValue(":shortlink", shortlink);
        query.exec();
        if (query.next())
            QMessageBox::critical(this, "Ошибка", "Попробуйте другую сокращенную ссылку");
        else{
           // QSqlQuery query;
                query.prepare("INSERT INTO links(longlink, shortlink) VALUES (:longlink, :shortlink) ");
                query.bindValue(":longlink", longlink);
                query.bindValue(":shortlink", shortlink);
          if(!query.exec())
               QMessageBox::critical(this, "Ошибка", "Подключение к базе данных не выполнено");
          else {
             // QSqlQuery query;

          if(!query.exec("SELECT * FROM links ORDER BY id DESC LIMIT 1"))
                       QMessageBox::critical(this, "Ошибка", "Error error");

          ui->verticalLayout->setAlignment(Qt::AlignTop);
          while(query.next()){

               QCommandLinkButton* shortlink = new QCommandLinkButton(query.value("shortlink").toString());

               shortlink->setToolTip("Перейти по ссылке: " + query.value("longlink").toString());

               QObject::connect(shortlink, &QPushButton::clicked, this, &MainWindow::open_url);
               QFrame* line = new QFrame(this);

               line->setFrameShape(QFrame::HLine);

               shortlink->setProperty("class", "shortlink");

               ui->verticalLayout->addWidget(shortlink);
               ui->verticalLayout->addWidget(line);
               }
             ui->line_longlink->setText("");
             ui->line_shortlink->setText("");
             ui->create_link->setText("Запись добавлена");
         }
    }
        }








  db.close();
   }





void MainWindow::keyPressEvent(QKeyEvent *e)
{
    if(e->key() == Qt::Key_Enter){
        ui->create_link->click();
    }
}














void MainWindow::on_scrollArea_customContextMenuRequested(const QPoint &pos)
{

}


void MainWindow::on_commandLinkButton_clicked()
{
   /* QString name = qobject_cast<QCommandLinkButton*>(sender())->toolTip(); // получаем данные
    QStringList full_url = name.split(": ");
    // Если нет слова "http" в ссылке, то добавляем его
    if(!full_url.contains("http")) full_url[1] = "http://" + full_url[1];
    QDesktopServices::openUrl(QUrl(full_url[1])); */
}





